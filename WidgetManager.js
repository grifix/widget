"use strict";

export default class WidgetManager {

    /**
     * @type {{}}
     */
    #widgetDefinitions

    /**
     *
     * @param {{}} widgetDefinitions
     */
    constructor(widgetDefinitions) {
        this.#widgetDefinitions = widgetDefinitions;
    }

    /**
     *
     * @param {HTMLElement=}rootElement
     */
    bindWidgets(rootElement) {
        const that = this;
        if (!rootElement) {
            rootElement = document.body;
        }

        rootElement.querySelectorAll('[data-widget]').forEach(
            function (element) {
                if (!(element instanceof HTMLElement)) {
                    return;
                }
                const widgetName = element.getAttribute('data-widget');
                const widget = that.createWidget(widgetName, element);
                widget.init();
            });
    }


    /**
     * @param {String} widgetName
     * @return {Object}
     */
    #getWidgetClass(widgetName) {
        const result = this.#widgetDefinitions[widgetName];
        if (!result) {
            throw new Error('Widget ' + widgetName + ' does not exist!');
        }
        return result;
    }

    /**
     * @param {String} name
     * @param {HTMLElement} element
     * @param {{}=} listeners
     * @param {{}=} options
     * @return {Widget}
     */
    createWidget(name, element, listeners, options) {
        const widgetClass = this.#getWidgetClass(name);
        return new widgetClass(element, name, this, listeners, options);
    }
}

