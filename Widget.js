"use strict";

export default class Widget {
    /**
     * @type {HTMLElement}
     * @protected
     */
    _element

    /**
     * @type {{}}
     * @private
     */
    _options = {};

    /**
     * @type {WidgetManager}
     * @protected
     */
    _widgetManager

    /**
     * @type {String}
     * @protected
     */
    _name

    /**
     * @type {{}}
     * @protected
     */
    _eventListeners = {}

    /**
     * @param {HTMLElement} element
     * @param {String} name
     * @param {WidgetManager} widgetManager
     * @param {Object|null=} listeners
     * @param {Object|null=} options
     */
    constructor(
        element,
        name,
        widgetManager,
        listeners,
        options
    ) {
        this._element = element;
        this._widgetManager = widgetManager;
        this._name = name;

        if (listeners) {
            this.#attachEventListeners(listeners);
        }

        if (!options) {
            options = {};
        }

        if (options) {
            this._options = {...options, ...this.#getOptionsFromElement()};
        }
        this._doConstruct();
    }

    /**
     * @returns {void}
     */
    init() {
        if (this._isInitialized()) {
            return;
        }
        this._widgetManager.bindWidgets(this._element);
        this._element.grifix_widget = this;
        this._doInit();
        this._dispatchEvent('WidgetInitialized', new WidgetInitializedEvent(this._name));
    }

    /**
     * @returns {String}
     */
    getClassName() {
        return this.constructor.name;
    }

    /**
     *
     * @returns {void}
     * @protected
     */
    _doInit() {

    }

    /**
     *
     * @protected
     * @returns {void}
     */
    _doConstruct() {

    }

    /**
     * @returns {Object}
     */
    #getOptionsFromElement() {
        if (this._element.hasAttribute('data-options')) {
            return JSON.parse(this._element.getAttribute('data-options'));
        }
        return {};
    }

    /**
     * @returns {boolean}
     * @protected
     */
    _isInitialized() {
        // noinspection RedundantIfStatementJS
        if (this._element['grifix_widget']) {
            return true;
        }
        return false;
    }

    /**
     * @param {String=} sourceUrl
     * @returns {Promise<void>}
     */
    async reload(sourceUrl) {

        if (!sourceUrl) {
            sourceUrl = this._getSourceUrl();
        }
        const div = document.createElement('div');
        div.innerHTML = await this.#fetchSource(sourceUrl);
        this._element.innerHTML = div.querySelector(':first-child').innerHTML;
        this._element.grifix_widget = null;
        this.init();
    }

    /**
     * @param listeners
     * @returns {Object}
     */
    #attachEventListeners(listeners) {
        for (const [eventName, listener] of Object.entries(listeners)) {
            this.on(eventName, listener);
        }
    }

    /**
     *
     * @param {String} eventName
     * @param {Function} listener
     */
    on(eventName, listener) {
        if (!this._eventListeners[eventName]) {
            this._eventListeners[eventName] = [];
        }
        this._eventListeners[eventName].push(listener);
    }

    /**
     *
     * @return {String}
     * @protected
     */
    _getSourceUrl() {
        return this._element.getAttribute('data-source_url');
    }

    /**
     * @param {String} name
     * @param {Object} event
     * @returns {void}
     * @protected
     */
    _dispatchEvent(name, event) {
        if (!this._eventListeners[name]) {
            return;
        }
        this._eventListeners[name].forEach(function (listener) {
            listener(event);
        })
    }

    /**
     * @param {String|null} sourceUrl
     * @returns {Promise<string>}
     */
    async #fetchSource(sourceUrl) {
        if (!sourceUrl) {
            sourceUrl = this._getSourceUrl();
        }
        if (!sourceUrl) {
            throw new Error('Source url is not defined for widget [' + this._name + ']!');
        }
        const response = await fetch(sourceUrl);
        if (response.ok) {
            return response.text()
        }
        throw new Error('Cannot fetch source!')
    }


    /**
     *
     * @param {String} name
     * @protected
     * @returns HTMLElement
     */
    _getElement(name) {
        const result = this._element.querySelector('[data-role="' + name + '"]');
        if (!result) {
            throw new Error('Element [' + name + '] does not exist for widget [' + this._name + ']!');
        }
        return result;
    }


    /**
     * @param name
     * @returns {NodeListOf<HTMLElement>}
     * @protected
     */
    _findElements(name) {
        return this._element.querySelectorAll('[data-role="' + name + '"]');
    }

    /**
     * @param {String} name
     * @return {Widget[]}
     * @protected
     */
    _findWidgets(name) {
        let result = [];
        this._element.querySelectorAll('[data-widget="' + name + '"]').forEach(
            function (element) {
                result.push(element.grifix_widget);
            }
        )
        return result;
    }

    /**
     *
     * @param {String} name
     * @protected
     * @return Widget
     */
    _getWidget(name) {
        return this._element.querySelector('[data-widget="' + name + '"]').grifix_widget;
    }
}

export class WidgetInitializedEvent {
    /**
     * @type {String}
     */
    widgetName

    /**
     *
     * @param {String} widgetName
     */
    constructor(widgetName) {
        this.widgetName = widgetName;
    }
}
